﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SearchInfoOnEnterprise.DAL.EF;
using SearchInfoOnEnterprise.DAL.Entities;
using SearchInfoOnEnterprise.DAL.Interfaces;

namespace SearchInfoOnEnterprise.DAL.Repositories
{
	public class EFUnitOfWork : IUnitOfWork
	{
		private InfoOnEnterpriseContext db;
		private InfoOnEnterpriseRepository infoOnEnterpriseRepository;
		private InformationAboutRequestRepository informationAboutRequestRepository;

		public EFUnitOfWork(string connectionString)
		{
			db = new InfoOnEnterpriseContext(connectionString);
		}
		public IRepository<InfoOnEnterprise> InfoOnEnterprises
		{
			get
			{
				if (infoOnEnterpriseRepository == null)
					infoOnEnterpriseRepository = new InfoOnEnterpriseRepository(db);
				return infoOnEnterpriseRepository;
			}
		}

		public IRepository<InformationAboutRequest> InformationAboutRequests
		{
			get
			{
				if (informationAboutRequestRepository == null)
					informationAboutRequestRepository = new InformationAboutRequestRepository(db);
				return informationAboutRequestRepository;
			}
		}

		public void Save()
		{
			db.SaveChanges();
		}

		private bool disposed = false;

		public virtual void Dispose(bool disposing)
		{
			if (!this.disposed)
			{
				if (disposing)
				{
					db.Dispose();
				}
				this.disposed = true;
			}
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}
	}
}
