﻿angular.module("exampleApp", [])
.constant("baseUrl", "http://localhost:60397/api/values/")
.controller("defaultCtrl", function ($scope, $http, baseUrl) {

    // Текущее педставление
    $scope.currentView = "search";
  
    // 
    $scope.sendingAndReceivingData = function (currentItem) {
        // Отправка GET запроса для получения данных.   
        $http({
            url: baseUrl + currentItem.name,
            method: "GET",
        }).success(function (modifiedItem) {
            $scope.items = modifiedItem;
            $scope.currentView = "table";
        });
    }

    // Очистка строки поиска и возврат к поиску.
    $scope.backToSearch = function () {
        $scope.currentItem = {};
        $scope.currentView = "search";
    }
});