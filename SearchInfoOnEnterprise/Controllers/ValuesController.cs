﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SearchInfoOnEnterprise.Models;
using System.Text.RegularExpressions;
using System.IO;
using System.Text;
using AutoMapper;
using SearchInfoOnEnterprise.BLL.DTO;
using System.Web.Mvc;
using SearchInfoOnEnterprise.BLL.Interfaces;
using SearchInfoOnEnterprise.BLL.Services;

namespace SearchInfoOnEnterprise.Controllers
{
	public class ValuesController : ApiController
	{
		IInfoOnEnterpriseService infoOnEnterpriseService;
		IInformationAboutRequestService informationAboutRequestService;
		public ValuesController(IInfoOnEnterpriseService serv, IInformationAboutRequestService serv1)
		{
			infoOnEnterpriseService = serv;
			informationAboutRequestService = serv1;
		}

		public IEnumerable<InfoOnEnterpriseViewModel> GetInfoEnterprise(string id)
		{
			informationAboutRequestService.InformationAboutRequestAddToDatabase(new InformationAboutRequestDTO { Request = id });
			IEnumerable<InfoOnEnterpriseDTO> infoOnEnterpriseDtos = infoOnEnterpriseService.GetInfoOnEnterprise(id);
			Mapper.Initialize(cfg => cfg.CreateMap<InfoOnEnterpriseDTO, InfoOnEnterpriseViewModel>());
			var ListResponse = Mapper.Map<IEnumerable<InfoOnEnterpriseDTO>, List<InfoOnEnterpriseViewModel>>(infoOnEnterpriseDtos);
			
			return ListResponse;
		}
		
	}
	
}
