﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SearchInfoOnEnterprise.BLL.DTO
{
	public class InformationAboutRequestDTO
	{
		public int Id { get; set; }
		public string Request { get; set; }
		public string DateOfRequest { get; set; }
	}
}
