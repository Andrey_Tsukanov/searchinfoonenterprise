﻿namespace SearchInfoOnEnterprise.DAL.EF
{
	using System;
	using System.Data.Entity;
	using System.Linq;
	using SearchInfoOnEnterprise.DAL.Entities;

	public class InfoOnEnterpriseContext : DbContext
	{
		// Контекст настроен для использования строки подключения "InfoOnEnterpriseContext" из файла конфигурации  
		// приложения (App.config или Web.config). По умолчанию эта строка подключения указывает на базу данных 
		// "SearchInfoOnEnterprise.DAL.EF.InfoOnEnterpriseContext" в экземпляре LocalDb. 
		// 
		// Если требуется выбрать другую базу данных или поставщик базы данных, измените строку подключения "InfoOnEnterpriseContext" 
		// в файле конфигурации приложения.
		//static InfoOnEnterpriseContext()
		//{
		//	Database.SetInitializer(new InfoOnEnterpriseDbInitialize());
		//}
		public InfoOnEnterpriseContext(string connectionString)
			: base(connectionString)//"name=InfoOnEnterpriseContext"
		{
		}
		public DbSet<InfoOnEnterprise> InfoOnEnterprises { get; set; }
		public DbSet<InformationAboutRequest> InformationAboutRequests { get; set; }
	}
	public class InfoOnEnterpriseDbInitialize : DropCreateDatabaseAlways<InfoOnEnterpriseContext>
	{
		protected override void Seed(InfoOnEnterpriseContext db)
		{
			db.InfoOnEnterprises.Add(new InfoOnEnterprise { Name = "Техно", Description = "производство труб и т. п.", Mail = "enterprise@mail.ru", PhoneNumber = "43-43-433" });
			db.InfoOnEnterprises.Add(new InfoOnEnterprise { Name = "Техно1", Description = "производство труб и т. п.", Mail = "enterprise@mail.ru", PhoneNumber = "43-43-433" });
			db.InfoOnEnterprises.Add(new InfoOnEnterprise { Name = "Техно2", Description = "производство труб и т. п.", Mail = "enterprise@mail.ru", PhoneNumber = "43-43-433" });
			base.Seed(db);
		}
	}

}