﻿using SearchInfoOnEnterprise.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SearchInfoOnEnterprise.BLL.DTO;
using SearchInfoOnEnterprise.DAL.Entities;
using SearchInfoOnEnterprise.DAL.Interfaces;

namespace SearchInfoOnEnterprise.BLL.Services
{
	public class InformationAboutRequestService : IInformationAboutRequestService
	{
		IUnitOfWork Database { get; set; }
		public InformationAboutRequestService (IUnitOfWork uow)
		{
			Database = uow;
		}
		public void Dispose()
		{
			Database.Dispose();
		}

		public void InformationAboutRequestAddToDatabase(InformationAboutRequestDTO informationAboutRequestDto)
		{
			InformationAboutRequest informationAboutRequest = new InformationAboutRequest
			{
				Request = informationAboutRequestDto.Request,
				DateOfRequest = DateTime.Now.ToString()
			};
			Database.InformationAboutRequests.Create(informationAboutRequest);
			Database.Save();
		}
	}
}
