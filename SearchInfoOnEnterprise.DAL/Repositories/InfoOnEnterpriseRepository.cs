﻿using SearchInfoOnEnterprise.DAL.EF;
using SearchInfoOnEnterprise.DAL.Entities;
using SearchInfoOnEnterprise.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace SearchInfoOnEnterprise.DAL.Repositories
{
	public class InfoOnEnterpriseRepository : IRepository <InfoOnEnterprise>
	{
		private InfoOnEnterpriseContext db;

		public InfoOnEnterpriseRepository(InfoOnEnterpriseContext context)
		{
			this.db = context;
		}

		public IEnumerable<InfoOnEnterprise> GetAll()
		{
			return db.InfoOnEnterprises;
		}

		public InfoOnEnterprise Get(int id)
		{
			return db.InfoOnEnterprises.Find(id);
		}

		public void Create(InfoOnEnterprise item)
		{
			db.InfoOnEnterprises.Add(item);
		}

		public void Update(InfoOnEnterprise item)
		{
			db.Entry(item).State = EntityState.Modified;
		}

		public IEnumerable<InfoOnEnterprise> Find(Func<InfoOnEnterprise, Boolean> predicate)
		{
			return db.InfoOnEnterprises.Where(predicate).ToList();
		}

		public void Delete(int id)
		{
			InfoOnEnterprise item = db.InfoOnEnterprises.Find(id);
			if (item != null)
				db.InfoOnEnterprises.Remove(item);
		}
	}
}
