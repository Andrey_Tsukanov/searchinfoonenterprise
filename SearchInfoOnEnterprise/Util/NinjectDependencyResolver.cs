﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ninject;
using SearchInfoOnEnterprise.BLL.Interfaces;
using SearchInfoOnEnterprise.BLL.Services;
using System.Web.Mvc;

namespace SearchInfoOnEnterprise.Util
{
	public class NinjectDependencyResolver : IDependencyResolver
	{
		private IKernel kernel;
		public NinjectDependencyResolver(IKernel kernelParam)
		{
			kernel = kernelParam;
			AddBindings();
		}
		public object GetService(Type serviceType)
		{
			return kernel.TryGet(serviceType);
		}
		public IEnumerable<object> GetServices(Type serviceType)
		{
			return kernel.GetAll(serviceType);
		}
		private void AddBindings()
		{
			kernel.Bind<IInfoOnEnterpriseService>().To<InfoOnEnterpriseService>();
			kernel.Bind<IInformationAboutRequestService>().To<InformationAboutRequestService>();
		}
	}
}