﻿using SearchInfoOnEnterprise.BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SearchInfoOnEnterprise.BLL.Interfaces
{
	 public interface IInformationAboutRequestService
	{
		void InformationAboutRequestAddToDatabase(InformationAboutRequestDTO InformationAboutRequestDto);
		void Dispose();
	}
}
