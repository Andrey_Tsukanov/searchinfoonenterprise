﻿using AutoMapper;
using SearchInfoOnEnterprise.BLL.DTO;
using SearchInfoOnEnterprise.BLL.Interfaces;
using SearchInfoOnEnterprise.DAL.Entities;
using SearchInfoOnEnterprise.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SearchInfoOnEnterprise.BLL.Services
{
	public class InfoOnEnterpriseService : IInfoOnEnterpriseService
	{
		IUnitOfWork Database { get; set; }
		string BaseUrl = "Для запуска приложения вставьте сюда URL для запросов из Яндекс.XML...&query=Сайт+предприятия+";

		string TemplateRegexUrl = @"<url>\S*</url>";
		string TemplateRegexPhone = @"\d{3}-\d{2}-\d{2}";
		string TemplateRegexMail = @"[A-Za-z]+[\.A-Za-z0-9_-]*[A-Za-z0-9]+@[A-Za-z]+\.[A-Za-z]+";
		string TemplateRegexDescription = @"<title>.*?</title>";
		string TemplateRegexCharset = @"<charset>\S*</charset>";

		string defaultMessage = "Информация отсутствует";

		public InfoOnEnterpriseService(IUnitOfWork uow)
		{
			Database = uow;
		}

		void InfoOnEnterpriseAddToDatabase(InfoOnEnterpriseDTO infoOnEnterpriseDto)
		{
			InfoOnEnterprise infoOnEnterprise = new InfoOnEnterprise
			{
				Name = infoOnEnterpriseDto.Name,
				PhoneNumber = infoOnEnterpriseDto.PhoneNumber,
				Mail = infoOnEnterpriseDto.Mail,
				Description = infoOnEnterpriseDto.Description
			};
			Database.InfoOnEnterprises.Create(infoOnEnterprise);
			Database.Save();
		}

		public IEnumerable<InfoOnEnterpriseDTO> GetInfoOnEnterprise(string nameEnterprise)
		{
			var ListResponse = new List<InfoOnEnterpriseDTO>();
			string responseYandex = null;
			using (var client = new HttpClient())
			{
				var response = client.GetAsync(BaseUrl + nameEnterprise).Result;//Получаем ответ.
				responseYandex = response.Content.ReadAsStringAsync().Result; // Сериализуем ответ в строку.
			}
			var listUrl = GetItemsResponse(responseYandex, TemplateRegexUrl);
			var listCharset = GetItemsResponse(responseYandex, TemplateRegexCharset);
			if (listUrl == null)
			{
				ListResponse.Add(new InfoOnEnterpriseDTO
				{
					Name = "Информация недоступна",
					PhoneNumber = "Информация недоступна",
					Mail = "Информация недоступна",
					Description = "Информация недоступна"
				});
				return ListResponse;
			}
			for (int i = 0; i < listUrl.Count; i++)
			{
				var site = SendRequest(listUrl[i], listCharset[i]);
				var phone = GetItemsResponse(site, TemplateRegexPhone) != null ? GetItemsResponse(site, TemplateRegexPhone)[0] : defaultMessage;
				var mail = GetItemsResponse(site, TemplateRegexMail) != null ? GetItemsResponse(site, TemplateRegexMail)[0] : defaultMessage;
				var description = GetItemsResponse(site, TemplateRegexDescription) != null ? GetItemsResponse(site, TemplateRegexDescription)[0] : defaultMessage;
				var infoOnEnterpriseDTO = new InfoOnEnterpriseDTO
				{
					Name = nameEnterprise,
					PhoneNumber = phone,
					Mail = mail,
					Description = description
				};
				InfoOnEnterpriseAddToDatabase(infoOnEnterpriseDTO);
			
				ListResponse.Add(infoOnEnterpriseDTO);
			}

			return ListResponse;

		}


		string SendRequest(string url, string charset = "utf-8")
		{
			Encoding encoding = Encoding.GetEncoding(charset);
			HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create(url);
			req.Timeout = 700;
			try
			{
				HttpWebResponse resp = (HttpWebResponse)req.GetResponse();


				using (StreamReader stream = new StreamReader(
					 resp.GetResponseStream(), encoding))
				{
					return stream.ReadToEnd();
				}
			}
			catch (Exception)
			{

				return "";
			}
		}
		List<string> GetItemsResponse(string XmlOrHtmlString, string TemplateRegex)
		{
			var resultline = "";
			var listResponses = new List<string>();
			Regex regex = new Regex(TemplateRegex);
			MatchCollection matches = regex.Matches(XmlOrHtmlString);
			if (matches.Count <= 0)
			{
				return null;
			}
			foreach (Match match in matches)
			{
				if (TemplateRegex[0] == '<')
				{
					var firstIndex = match.Value.IndexOf('>');
					var lastIndex = match.Value.LastIndexOf('<');
					resultline = match.Value.Substring(firstIndex + 1, lastIndex - firstIndex - 1);
					listResponses.Add(resultline);
				}
				else
				{
					resultline = match.Value;
					listResponses.Add(resultline);
				}
			}
			return listResponses;
		}

		public void Dispose()
		{
			Database.Dispose();
		}
	}
}
