﻿using SearchInfoOnEnterprise.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SearchInfoOnEnterprise.DAL.Interfaces
{
	public interface IUnitOfWork : IDisposable
	{
		IRepository<InfoOnEnterprise> InfoOnEnterprises { get; }
		IRepository<InformationAboutRequest> InformationAboutRequests { get; }
		void Save();
	}
}
