﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SearchInfoOnEnterprise.Models
{
	public class InformationAboutRequestViewModel
	{
		public int Id { get; set; }
		public string Request { get; set; }
		public string DateOfRequest { get; set; }
	}
}