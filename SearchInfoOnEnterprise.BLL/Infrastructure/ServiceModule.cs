﻿using Ninject.Modules;
using SearchInfoOnEnterprise.DAL.Interfaces;
using SearchInfoOnEnterprise.DAL.Repositories;
using System;


namespace SearchInfoOnEnterprise.BLL.Infrastructure
{
	public class ServiceModule : NinjectModule
	{
		private string connectionString;

		public ServiceModule(string connection)
		{
			connectionString = connection;
		}
		public override void Load()
		{
			Bind<IUnitOfWork>().To<EFUnitOfWork>().WithConstructorArgument(connectionString);
		}
	}
}
