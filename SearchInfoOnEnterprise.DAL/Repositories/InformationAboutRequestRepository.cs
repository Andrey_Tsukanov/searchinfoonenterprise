﻿using System;
using System.Collections.Generic;
using System.Linq;
using SearchInfoOnEnterprise.DAL.EF;
using SearchInfoOnEnterprise.DAL.Entities;
using SearchInfoOnEnterprise.DAL.Interfaces;
using System.Data.Entity;

namespace SearchInfoOnEnterprise.DAL.Repositories
{
	public class InformationAboutRequestRepository : IRepository<InformationAboutRequest>
	{
		private InfoOnEnterpriseContext db;

		public InformationAboutRequestRepository(InfoOnEnterpriseContext context)
		{
			this.db = context;
		}

		public IEnumerable<InformationAboutRequest> GetAll()
		{
			return db.InformationAboutRequests;
		}

		public InformationAboutRequest Get(int id)
		{
			return db.InformationAboutRequests.Find(id);
		}

		public void Create(InformationAboutRequest item)
		{
			db.InformationAboutRequests.Add(item);
		}

		public void Update(InformationAboutRequest item)
		{
			db.Entry(item).State = EntityState.Modified;
		}

		public IEnumerable<InformationAboutRequest> Find(Func<InformationAboutRequest, Boolean> predicate)
		{
			return db.InformationAboutRequests.Where(predicate).ToList();
		}

		public void Delete(int id)
		{
			InformationAboutRequest item = db.InformationAboutRequests.Find(id);
			if (item != null)
				db.InformationAboutRequests.Remove(item);
		}
	}
}
