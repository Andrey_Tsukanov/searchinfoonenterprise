﻿using SearchInfoOnEnterprise.BLL.Interfaces;
using System;
using System.Web.Mvc;

namespace SearchInfoOnEnterprise.Controllers
{
	public class HomeController : Controller
	{
		
		public ActionResult Index()
		{
			ViewBag.Title = "Информация о предприятиях";

			return View();
		}
	}
}
